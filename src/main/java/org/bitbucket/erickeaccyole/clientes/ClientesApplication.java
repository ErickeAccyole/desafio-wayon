package org.bitbucket.erickeaccyole.clientes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientesApplication {
	
	/*
	 * @Bean public CommandLineRunner run (@Autowired ClienteRepository repository)
	 * { return args -> { Cliente cliente = new Cliente();
	 * cliente.setCpf("00000000000"); cliente.setNome("Fulano");
	 * 
	 * repository.save(cliente); }; }
	 */
	
	public static void main(String[] args) {
		SpringApplication.run(ClientesApplication.class, args);
	}
}
