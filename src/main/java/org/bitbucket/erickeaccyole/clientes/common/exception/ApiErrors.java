package org.bitbucket.erickeaccyole.clientes.common.exception;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class ApiErrors  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<String> errors;
	
	public ApiErrors() {
	}
	

	public ApiErrors(List<String> errors) {
		this.errors = errors;
	}
	
	public ApiErrors(String message) {
		this.errors = Arrays.asList(message);
	}

	
	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
}
