package org.bitbucket.erickeaccyole.clientes.common.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Funcao {

	private static String formatLocalDateTimeToDataBaseStyle(LocalDateTime localDateTime) {
		return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(localDateTime);
	}
	
	public static void logInfo(String mensagem) {		
		System.out.println(formatLocalDateTimeToDataBaseStyle(LocalDateTime.now()) + ": " + mensagem);
	}

}
