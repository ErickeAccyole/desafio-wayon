package org.bitbucket.erickeaccyole.clientes.model;

import java.util.List;

import javax.validation.Valid;

import org.bitbucket.erickeaccyole.clientes.dto.ClienteRequestDTO;
import org.bitbucket.erickeaccyole.clientes.entity.Cliente;
import org.bitbucket.erickeaccyole.clientes.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

@Component
public class ClienteModel {
	
	@Autowired
	private ClienteRepository repository;
	
	public Cliente salvar(ClienteRequestDTO clienteRequest) throws Exception {
		Cliente cliente = clienteRequest.toModel();

		validaCadastroCliente(cliente);
		
		return repository.save(cliente);
	}
	
	public List<Cliente> buscarTodos() {
		return repository.findAll();
	}

	public Cliente buscarPorId(Integer id) {
		return repository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente não encontrado!"));
	}
	

	public void deletar(@PathVariable Integer id) {
		repository
			.findById(id)
			.map(cliente -> {
				repository.delete(cliente);
				return Void.TYPE;
			})
			.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente não encontrado!"));
	}
	

	public void atualizar(@PathVariable Integer id, @RequestBody @Valid ClienteRequestDTO clienteRequest) {
		Cliente clienteAtualizado = clienteRequest.toModel();
		
		validaAtualizacaoCliente(id, clienteRequest.getCpf());
		
		clienteAtualizado.setId(id);
		repository.save(clienteAtualizado);
	}
	
	private void validaCadastroCliente(Cliente cliente) {
		repository.findByCpf(cliente.getCpf())
			.ifPresent(cli -> {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Já existe um usuário cadastrado com esse cpf!");
			});
	}
	
	private void validaAtualizacaoCliente(Integer id, String cpf) {
		Cliente clienteConsulta = repository.findById(id)
			.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente não encontrado!"));
		
		Cliente clienteConsultaCpf = repository.findByCpf(cpf).orElse(null);
		if (clienteConsultaCpf != null) {
			if (clienteConsulta.getId() != clienteConsultaCpf.getId()) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Já existe um usuário cadastrado com esse cpf!");			
			}			
		}
	}
}
