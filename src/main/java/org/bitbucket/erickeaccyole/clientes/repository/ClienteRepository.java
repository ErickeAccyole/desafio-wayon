package org.bitbucket.erickeaccyole.clientes.repository;

import java.util.Optional;

import org.bitbucket.erickeaccyole.clientes.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
	
	Optional<Cliente> findByCpf(String cpf);
	
	Optional<Cliente> findByIdAndCpf(Integer id, String cpf);

}
