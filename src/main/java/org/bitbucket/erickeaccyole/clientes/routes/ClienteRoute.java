package org.bitbucket.erickeaccyole.clientes.routes;

import java.util.List;

import javax.validation.Valid;

import org.bitbucket.erickeaccyole.clientes.common.utils.Funcao;
import org.bitbucket.erickeaccyole.clientes.dto.ClienteRequestDTO;
import org.bitbucket.erickeaccyole.clientes.entity.Cliente;
import org.bitbucket.erickeaccyole.clientes.model.ClienteModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api
@RestController
@RequestMapping("/api/clientes")
public class ClienteRoute {
	
	@Autowired
	private ClienteModel clienteModel;
	
	@ApiOperation(value = "Salva um cliente na base de dados!")
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@ApiResponses(value = {
			@ApiResponse(code=201, message = ""),
			@ApiResponse(code=400, message = "Bad Request")
	})
	public Cliente salvar(@RequestBody @Valid ClienteRequestDTO cliente) throws Exception {
		Funcao.logInfo("[CLIENTES] - [/POST] HTTP Request :: salvar method");

		return clienteModel.salvar(cliente);
	}
	
	@ApiOperation(value = "Recupera um cliente por Id!")
	@GetMapping("{id}")
	public Cliente buscarPorId(@PathVariable Integer id) {
		Funcao.logInfo("[CLIENTES] - [/GET] HTTP Request :: buscarPorId method");

		return clienteModel.buscarPorId(id);
	}
	
	@ApiOperation(value = "Recupera todos os clientes na base de dados")
	@ResponseStatus(HttpStatus.OK)
	@GetMapping
	public List<Cliente> buscarTodos() throws Exception {
		Funcao.logInfo("[CLIENTES] - [/GET] HTTP Request :: buscarTodos method");
				
		return clienteModel.buscarTodos();
	}
	
	@ApiOperation(value = "Remove um cliente da base de dados!")
	@DeleteMapping("{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletar(@PathVariable Integer id) {
		Funcao.logInfo("[CLIENTES] - [/DELETE] HTTP Request :: deletar method");

		clienteModel.deletar(id);
	}
	
	@ApiOperation(value = "Atualiza um cliente na base de dados!")
	@PutMapping("{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiResponses(value = {
			@ApiResponse(code=204, message = "No Content"),
			@ApiResponse(code=400, message = "Bad Request")
	})
	public void atualizar(@PathVariable Integer id, @RequestBody @Valid ClienteRequestDTO clienteAtualizado) {
		Funcao.logInfo("[CLIENTES] - [/PUT] HTTP Request :: deletar atualizar");

		clienteModel.atualizar(id, clienteAtualizado);
	}
}
