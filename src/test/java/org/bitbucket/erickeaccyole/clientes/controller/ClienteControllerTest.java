package org.bitbucket.erickeaccyole.clientes.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.bitbucket.erickeaccyole.clientes.common.exception.ApiErrors;
import org.bitbucket.erickeaccyole.clientes.dto.ClienteRequestDTO;
import org.bitbucket.erickeaccyole.clientes.entity.Cliente;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase
class ClienteControllerTest {

	@Autowired
	private TestRestTemplate testRestTemplate;
	private final String url = "/api/clientes";
	
	@Test
	@DisplayName("Deve cadastrar quando todos os dados validos")
	void test1() {
		ClienteRequestDTO request = new ClienteRequestDTO();
		request.setNome("Ericke Teste");
		request.setCpf("08944414424");
		
		ResponseEntity<Cliente> response = testRestTemplate.postForEntity(url, request, Cliente.class);

		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertTrue(response.getBody().getId() > 0);
	}
	
	@Test
	@DisplayName("Não deve cadastrar quando o cpf for inválido")
	void test2() {
		ClienteRequestDTO request = new ClienteRequestDTO();
		request.setNome("Ericke Teste");
		request.setCpf("089444144244");

		ResponseEntity<ApiErrors> response = testRestTemplate.postForEntity(url, request, ApiErrors.class);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		assertEquals(1, response.getBody().getErrors().size());
		assertEquals("CPF inválido.", response.getBody().getErrors().get(0));

	}
	
	@Test
	@DisplayName("Não deve cadastrar quando o cpf for duplicado na base de dados")
	void test3() {
		ClienteRequestDTO request = new ClienteRequestDTO();
		request.setNome("Maria");
		request.setCpf("08944414424");

		ResponseEntity<ApiErrors> response = testRestTemplate.postForEntity(url, request, ApiErrors.class);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		assertEquals(1, response.getBody().getErrors().size());
		assertTrue(response.getBody().getErrors().get(0).contains("Já existe um usuário cadastrado com esse cpf!"));
	}
	
	@Test
	@DisplayName("Não deve cadastrar quando o nome for nulo")
	void test4() {
		ClienteRequestDTO request = new ClienteRequestDTO();
		request.setNome(null);
		request.setCpf("08944414424");

		ResponseEntity<ApiErrors> response = testRestTemplate.postForEntity(url, request, ApiErrors.class);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
		assertEquals(1, response.getBody().getErrors().size());
		assertEquals("O campo nome é obrigatório.", response.getBody().getErrors().get(0));

	}
	
	@Test
	@DisplayName("Deve encontrar o cliente por id")
	void test5() {		
		ResponseEntity<Cliente> response = testRestTemplate.getForEntity(url + "/1", Cliente.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(1, response.getBody().getId());
	}
	
	@Test
	@DisplayName("Não deve encontar o cliente na base de dados")
	void test6() {
		ClienteRequestDTO request = new ClienteRequestDTO();
		request.setNome(null);
		request.setCpf("08944414424");

		ResponseEntity<ApiErrors> response = testRestTemplate.getForEntity(url + "/5", ApiErrors.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
		assertEquals(1, response.getBody().getErrors().size());
		assertTrue(response.getBody().getErrors().get(0).contains("Cliente não encontrado!"));
	}
	
	@Test
	@DisplayName("Deve atualizar o cliente quando todos os dados validos")
	void test7() {
		ClienteRequestDTO request = new ClienteRequestDTO();
		request.setNome("Ericke José");
		request.setCpf("08944414424");
		
		testRestTemplate.put(url + "/1", request);
		
		ResponseEntity<Cliente> response = testRestTemplate.getForEntity(url + "/1", Cliente.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(response.getBody().getNome(), request.getNome());		
	}
	
	@Test
	@DisplayName("Deve deletar quando o cliente existir")
	void test8() {
		testRestTemplate.delete(url+"/1");
		
		ResponseEntity<ApiErrors> response = testRestTemplate.getForEntity(url + "/1", ApiErrors.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
		assertEquals(1, response.getBody().getErrors().size());
		assertTrue(response.getBody().getErrors().get(0).contains("Cliente não encontrado!"));
	}
}
