# desafio-dev-api-rest

Projeto de desafio feito por Ericke José de Lacerda Accyole

## Passos para execução

Para que o projeto seja executado é necessário que:
 
 - Deve-se executar o `src\main\java\com\org.bitbucket.erickeaccyole.clientes\DespesasApplication.java` como uma aplicação java para iniciar a aplicação;
  

 - Seu aplicativo agora deve estar em execução em [localhost:8081](http://localhost:8081/sistema-clientes).
 
 - A aplicação se encarrega de levantar em memória o proprio banco de dados chamado de "db"
	 - Caso seja necessário deve ser mudado a senha e o usuário do banco em: `src\main\resources\application-development.properties`
	 
## O que foi feito

Foi criado uma API restfull que simula um cadastro de clientes.

## Testes

Para executar os testes unitários da API é necessário que:

 - Deve-se executar o `src\test\java\org.bitbucket.erickeaccyole.clientes\ClienteControllerTest.java` com JUnit Test;


## Documentação

A aplicação também disponibiliza a documentação da API usando o Swagger.

 - Documentação API: [http://localhost:8081/sistema-clientes/swagger-ui.html](http://localhost:8081/sistema-clientes/swagger-ui.html)
